#include <MC14051B.h>

int outputABCpins[] = {5, 6, 7};
MC14051B MCOutput(7, outputABCpins, 3);

int inputABCpins[] = {9, 10, 11};
MC14051B MCInput(8, inputABCpins, A0);

void setup (){}

void loop ()
{
  MCOutput.mcAnalogWrite(4, map(MCInput.mcAnalogRead(4), 0,1023,0,255));
  MCInput.reset();
}
