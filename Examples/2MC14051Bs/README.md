# MC14051B Library Example: 2 ICs

This example illustrates how you would use to MC14051Bs, one for input and one for output, connected to one Arduino. This gives you 8 input analog ports and 8 output PWM/"analog" ports.

![Schematics](fig04_MC14051B_IO_bb.png?raw=true "Schematics")

![Photo](fig05_pot_motor.jpg?raw=true "Photo")

See [a video of this MC14051B sketch working here](https://youtu.be/J6K8LxxtZKw).

## The Sketch


```
#include <MC14051B.h>

int outputABCpins[] = {5, 6, 7};
MC14051B MCOutput(7, outputABCpins, 3);

int inputABCpins[] = {9, 10, 11};
MC14051B MCInput(8, inputABCpins, A0);

void setup (){}

void loop ()
{
  MCOutput.mcAnalogWrite(4, map(MCInput.mcAnalogRead(4), 0,1023,0,255));
  MCInput.reset();
}
```